﻿using System;
using System.Collections.Generic;
using System.Fabric;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.ServiceFabric.Services.Communication.Runtime;
using Microsoft.ServiceFabric.Services.Runtime;
using System.Diagnostics;
using System.IO;
using Helpers;
using Microsoft.WindowsAzure.Storage.Table;
using Models;

namespace FfmpegWorker
{
    /// <summary>
    /// An instance of this class is created for each service instance by the Service Fabric runtime.
    /// </summary>
    internal sealed class FfmpegWorker : StatelessService
    {
        private readonly QueueManager _queueManager = new QueueManager();
        private readonly TableManager _tableManager = new TableManager();
        private readonly BlobManager _blobManager = new BlobManager();
        public FfmpegWorker(StatelessServiceContext context)
            : base(context)
        {
        }

        /// <summary>
        /// Optional override to create listeners (e.g., TCP, HTTP) for this service replica to handle client or user requests.
        /// </summary>
        /// <returns>A collection of listeners.</returns>
        protected override IEnumerable<ServiceInstanceListener> CreateServiceInstanceListeners()
        {
            return new ServiceInstanceListener[0];
        }

        /// <summary>        
        /// <param name="cancellationToken">Canceled when Service Fabric needs to shut down this service instance.</param>
        /// This is the main entry point for your service instance.
        /// </summary>
        protected override async Task RunAsync(CancellationToken cancellationToken)
        {
            await _queueManager.InitAsync();
            await _tableManager.InitAsync();
            await _blobManager.InitAsync();

            while (true)
            {
                cancellationToken.ThrowIfCancellationRequested();

                // Add a pool with max number of conversions.
                var message = await _queueManager.DequeueAsync();
                if (message != null)
                {
                    await _queueManager.DeleteAsync(message);
                    await DoWork(message.AsString);
                }

                await Task.Delay(TimeSpan.FromSeconds(1), cancellationToken);
            }
        }

        private async Task DoWork(string rowKey)
        {
            await Task.Run(async () =>
            {
                // Update database with status converting.
                var statusModel = await UpdateStatusModel(rowKey, Status.Fetching);
                // Fetch the file from storage to the temporary folder.
                var localInputFile = CreateInputFileName();
                await _blobManager.DownloadAsync(localInputFile, statusModel.RowKey);

                // Update database with status converting.
                statusModel = await UpdateStatusModel(rowKey, Status.Converting);
                // Convert file.
                var localOutputFile = Path.Combine(GetTempDirectory(), Path.GetTempFileName() + NameStrings.Webm);
                var succeded = await RunFfmpegConverter(localInputFile, localOutputFile);
                if (!succeded)
                {
                    // Converting did not succed
                    // Update database with status Error.
                    await UpdateStatusModel(rowKey, Status.Error);

                    // stop execution.
                    return;
                }

                // Update database with status publishing.
                statusModel = await UpdateStatusModel(rowKey, Status.Publishing);
                // Publish file in storage.
                statusModel.FileName = statusModel.FileName.Replace(NameStrings.Mp4, NameStrings.Webm);
                var publishUri = await Publish(localOutputFile, statusModel.FileName);

                // Update database with status Ready.
                await UpdateStatusModel(rowKey, Status.Ready, publishUri);

                // Remove temporary input and output files.
                await Task.Run(() => 
                {
                    File.Delete(localInputFile);
                    File.Delete(localOutputFile);
                });
                
            });
        }

        private async Task<string> Publish(string filePath, string blobName)
        {
            return await Task.Run(() => _blobManager.UploadFromLocalAsync(filePath, blobName));
        }

        private async Task<StatusModel> UpdateStatusModel(string id, Status status, string publishUri = null)
        {
            var retrieveOperation = TableOperation.Retrieve<StatusModel>("1", id);
            if ((await _tableManager.ExecuteAsync(retrieveOperation)).Result is StatusModel statusModel)
            {
                statusModel.PublishUri = publishUri ?? string.Empty;
                statusModel.Status = status.ToString();
                await _tableManager.ReplaceAsync(statusModel);
                return statusModel;
            }
            return null;
        }

        private async Task<bool> RunFfmpegConverter(string inputFile, string outputFile)
        {
            try
            {
                return await Task.Run(() =>
                {
                    // Start the process with the info we specified.
                    // Call WaitForExit and then the using-statement will close.
                    var ffmpegExePath = Path.Combine(GetDataPackage().Path, NameStrings.FfmpegExe);                    
                    var proc = new Process();
                    proc.StartInfo.FileName = ffmpegExePath;
                    proc.StartInfo.Arguments = "-i " + inputFile + " -c:v libvpx-vp9 -crf 30 -b:v 0 " + outputFile;
                    proc.StartInfo.RedirectStandardError = true;
                    proc.StartInfo.UseShellExecute = false;
                    if (!proc.Start())
                    {
                        return false;
                    }
                    var reader = proc.StandardError;
                    var line = string.Empty;
                    var lines = string.Empty;
                    while ((line = reader.ReadLine()) != null)
                    {
                        lines = lines + line;
                    }
                    proc.Close();

                    return true;
                });
            }
            catch
            {
                return await Task.FromResult<bool>(false);
            }
        }

        private static DataPackage GetDataPackage()
        {
            var activationContext = FabricRuntime.GetActivationContext();
            return activationContext.GetDataPackageObject(NameStrings.Data);
        }
        private static string GetTempDirectory()
        {
            var activationContext = FabricRuntime.GetActivationContext();
            return activationContext.TempDirectory;
        }

        private static string CreateInputFileName()
        {
            return Path.Combine(GetTempDirectory(), Guid.NewGuid().ToString() + NameStrings.Mp4); 
        }
    }
}
