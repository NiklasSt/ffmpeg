﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FfmpegWebApi.Helpers;
using Microsoft.WindowsAzure.Storage.Queue;
using Microsoft.Extensions.Configuration;
using Helpers;
using Microsoft.WindowsAzure.Storage.Table;
using Models;

namespace FfmpegWebApi.Controllers
{
    // RESTful API Designing guidelines — The best practices
    // https://hackernoon.com/restful-api-designing-guidelines-the-best-practices-60e1d954e7c9

    [Produces("application/json")]
    [Route("api/[controller]")]
    public class MoviesController : Controller
    {
        IConfiguration _configuration;
        QueueManager _queueManager;
        TableManager _tableManager;
        BlobManager _blobManager;

        public MoviesController(IConfiguration configuration, QueueManager queueManager,
            TableManager tableManager, BlobManager blobManger)
        {
            _configuration = configuration;
            _queueManager = queueManager;
            _tableManager = tableManager;
            _blobManager = blobManger;
        }

        // GET api/movies
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return await Task.Run(() =>
            {
                var rangeQuery = new TableQuery<StatusModel>().Where(TableQuery.CombineFilters(
                    TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, "1"),
                    TableOperators.And,
                    TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.GreaterThan, "0")));

                if (_tableManager.Table.ExecuteQuery(rangeQuery) is IEnumerable<StatusModel> statusModels)
                {
                    return Json(statusModels);
                }
                return Json("Database is empty !!!");
            });
        }

        // GET api/movies/{rowKey}
        [HttpGet("{rowKey}")]
        public async Task<IActionResult> Get(Guid rowKey)
        {
            var retrieveOperation = TableOperation.Retrieve<StatusModel>("1", rowKey.ToString());
            if ((await _tableManager.ExecuteAsync(retrieveOperation)).Result is StatusModel statusModel)
            {
                return Json(statusModel);
            }
            return Json($"No match could be found for id {rowKey}");
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

        // Wait for answer but leave back the thread.
        [HttpPost]
        [DisableFormValueModelBinding]
        //TODO: Add ValidateAntiForgeryToken
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Index()
        {
            StatusModel statusModel = new StatusModel(Guid.NewGuid().ToString());

            // Update database with status uploading.
            statusModel.Status = Status.Uploading.ToString();
            await _tableManager.InsertAsync(statusModel);

            // Get the stream to blob in azure storage.
            var blob = await _blobManager.GetBlockBlobReferenceAsync(statusModel.RowKey);
            using (var stream = await blob.OpenWriteAsync())
            {
                statusModel.UploadUri = blob.Uri.ToString();
                // Read stream from client and pipe/propagate it to the blob stream.

                statusModel.FileName = await Request.StreamFileAsync(stream);
            }

            // Update database with status queued.
            statusModel.Status = Status.Queued.ToString();
            await _tableManager.ReplaceAsync(statusModel);

            // Update message queue there is a new file to convert.
            CloudQueueMessage message = new CloudQueueMessage(statusModel.RowKey);
            await _queueManager.EnqueueAsync(message);

            return Ok(statusModel);
        }
    }
}
