﻿namespace Models
{
    public enum Status
    {
        None,
        Uploading,
        Queued,
        Fetching,
        Converting,
        Publishing,
        Ready,
        Error
    }
}
