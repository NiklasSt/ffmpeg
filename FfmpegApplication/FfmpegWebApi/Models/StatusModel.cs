﻿using Microsoft.WindowsAzure.Storage.Table;

namespace Models
{
    public class StatusModel : TableEntity
    {
        public StatusModel(string id)
        {
            PartitionKey = "1"; // For now we use 1
            RowKey = id; 
        }

        public StatusModel()
        {
            // Empty ctor required by azure storage table. 
        }

        //public string Id { get; set; }
        public string FileName { get; set; } = string.Empty;
        public string Status { get; set; } = string.Empty;
        public string UploadUri { get; set; } = string.Empty;
        public string PublishUri { get; set; } = string.Empty;
    }
}