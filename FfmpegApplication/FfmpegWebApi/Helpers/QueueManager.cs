﻿using Helpers;
using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using System.Threading.Tasks;

namespace Helpers
{
    public class QueueManager
    {
        CloudQueueClient _client;
        CloudQueue _queue;
        public async Task InitAsync()
        {
            var storageAccount = CloudStorageAccount.Parse(NameStrings.StorageConnectionString);

            // Create the queue client.
            _client = storageAccount.CreateCloudQueueClient();

            // Retrieve a reference to a queue.
            _queue = _client.GetQueueReference(NameStrings.MovieQueue);
            await _queue.CreateIfNotExistsAsync();
        }

        public async Task EnqueueAsync(CloudQueueMessage message)
        {
            await _queue.AddMessageAsync(message);
        }

        public async Task<CloudQueueMessage> DequeueAsync()
        {
            return (await _queue.GetMessageAsync());
        }

        public async Task<CloudQueueMessage> PeekMessageAsync()
        {
            return (await _queue.PeekMessageAsync());
        }

        public async Task DeleteAsync(CloudQueueMessage message)
        {
            await _queue.DeleteMessageAsync(message);
        }
    }
}
