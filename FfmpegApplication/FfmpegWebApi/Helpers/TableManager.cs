﻿using Helpers;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using System.Threading.Tasks;

namespace Helpers
{
    public class TableManager
    {
        CloudTableClient _client;
        public CloudTable Table { get; private set; }
        public async Task InitAsync()
        {
            var storageAccount = CloudStorageAccount.Parse(NameStrings.StorageConnectionString);

            // Create the queue client.
            _client = storageAccount.CreateCloudTableClient();

            // Retrieve a reference to a queue.
            Table = _client.GetTableReference(NameStrings.MovieStatusTable);

            await Table.CreateIfNotExistsAsync();
        }

        public async Task InsertAsync(ITableEntity entity)
        {
            var operation = TableOperation.Insert(entity);
            await InnerExecuteAsync(operation);
        }
     
        public async Task ReplaceAsync(ITableEntity entity)
        {
            var operation = TableOperation.Replace(entity);
            await InnerExecuteAsync(operation);
        }

        public async Task<TableResult> ExecuteAsync(TableOperation operation)
        {
            return await InnerExecuteAsync(operation);
        }

        private async Task<TableResult> InnerExecuteAsync(TableOperation operation)
        {
            return await Table.ExecuteAsync(operation); 
        }
    }
}
