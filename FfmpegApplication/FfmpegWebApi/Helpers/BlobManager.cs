﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.IO;
using System.Threading.Tasks;

namespace Helpers
{
    public class BlobManager
    {
        CloudBlobClient _client;
        CloudBlobContainer _container;
        public async Task InitAsync()
        {
            var storageAccount = CloudStorageAccount.Parse(NameStrings.StorageConnectionString);

            // Create the queue client.
            _client = storageAccount.CreateCloudBlobClient();

            // Retrieve a reference to a spcified blob container.
            _container = _client.GetContainerReference(NameStrings.MoviesContainerString);

            await _container.CreateIfNotExistsAsync();
        }

        public async Task<string> UploadFromLocalAsync(string localFilePath, string blobName)
        {
            // Retrieve reference to a blob in the container.
            var blockBlob = _container.GetBlockBlobReference(blobName);

            // Create or overwrite the blob with contents from a local file.
            using (var fileStream = File.OpenRead(localFilePath))
            {
                await blockBlob.UploadFromStreamAsync(fileStream);
            }

            return blockBlob.Uri.ToString();
        }

        public async Task DownloadAsync(string localFilePath, string blobName)
        {
            var blockBlob = _container.GetBlockBlobReference(blobName);

            using (var fileStream = File.OpenWrite(localFilePath))
            {
                await blockBlob.DownloadToStreamAsync(fileStream);
            }
        }

        public async Task<CloudBlockBlob> GetBlockBlobReferenceAsync(string fileName)
        {
            return await Task.Run(() => _container.GetBlockBlobReference(fileName));
        }
    }
}